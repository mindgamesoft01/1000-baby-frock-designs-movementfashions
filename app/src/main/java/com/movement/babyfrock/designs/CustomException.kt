package com.movement.babyfrock.designs

class CustomException(message: String) : Exception(message)
